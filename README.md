NumberEncoding
==========================================================

|Test Task              | Author            | Requirements
|-----------------------|-------------------|--------------
|Java Backend Engineer  |Qi Qi <qiq@kth.se> | JRE 7 or above

Two Ways to Run the Program:
==========================================================
1) By default, put the "input.txt" and "dictionary.txt" files under the same folder with the jar file "NumberEncoding-1.0-SNAPSHOT.jar". Run:

(The result "output.txt" will appear in the same folder)

```
java -jar NumberEncoding-1.0-SNAPSHOT.jar
```

2) Or specified the paths of the "input.txt", "dictionary.txt" and "output.txt" files. Run: 

```
java -jar NumberEncoding-1.0-SNAPSHOT.jar ./input.txt ./dictionary.txt ./output.txt
```