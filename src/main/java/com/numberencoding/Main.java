/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.numberencoding;

/**
 * This is the main class to run the program.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Main {

    public static void main(String[] args) {
        try {
            Encoder encoder;
            if (args.length == 3) {
                // [0]: input, [1]: dictionary, [2]: output
                encoder = new Encoder(args[0], args[1], args[2]);
            } else {
                // default file path
                encoder = new Encoder("input.txt", "dictionary.txt", "output.txt");
            }
            encoder.run();
            System.out.println("Number Encoding completed.");
        } catch (Exception ex) {
            System.out.println("Number Encoding Exception: " + ex);
        } finally {
            System.exit(0);
        }
    }
}
