/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.numberencoding;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class performs the encoding process and output the results.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Encoder {

    private final BufferedReader numberReader;  // input file reader
    private final PrintWriter resultWriter; // output file writer
    private final Map<String, List<String>> dictionaryMap;  // encode hashmap

    /*
     * Boolean array flags used to indicate current encoding digits,
     * so that single number can't be used if there's an valid encoding for 
     * the digit.
     */
    private boolean[] globalEncodedDigits;

    /**
     * Creates a new Encoder, given the name of the phone number and dictionary
     * files to read from, and the name of encoded result file to write to. The
     * dictionary will be read and compacted into the memory, but the phone
     * numbers will be encoded line by line.
     *
     * @param numberSrcPath The path of the phone number source file
     * @param dictionarySrcPath The path of the dictionary source file
     * @param resultDstPath The path of the encoded result destination file
     * @throws FileNotFoundException if the file "numberSrcPath" or
     * "dictionarySrcPath" does not exist, is a directory rather than a regular
     * file, or for some other reason cannot be opened for reading.
     * @throws IOException if the named file "resultDstPath" exists but is a
     * directory rather than a regular file, does not exist but cannot be
     * created, or cannot be opened for any other reason
     */
    public Encoder(String numberSrcPath, String dictionarySrcPath, String resultDstPath) throws FileNotFoundException, IOException {
        this.numberReader = new BufferedReader(new FileReader(numberSrcPath));
        this.resultWriter = new PrintWriter(new FileWriter(resultDstPath), true); // true ->  the println, printf, or format methods will flush the output buffer
        this.dictionaryMap = Util.loadDictionary(dictionarySrcPath);
    }

    /**
     * Execute the encoder.
     *
     * @throws IOException If the input file can't be read or the output file
     * can't be written
     */
    public void run() throws IOException {
        String srcNumber;
        while ((srcNumber = numberReader.readLine()) != null) {
            List<String> encodingResultList = new ArrayList<>();
            findEncodes(srcNumber, encodingResultList);
            for (String encoding : encodingResultList) {
                resultWriter.println(srcNumber + ": " + encoding); // println will automatically flush the output buffer
            }
        }
        resultWriter.close();
    }

    /**
     * The wrapper of the findEncodes method. It initiates the parameters for
     * the recursive findEncodes method. "srcNumber" is the raw string number
     * (e.g. "10/783--5") reading from the input file; "dstEncodingResultList"
     * is the result list storing all the valid encodes for the number.
     */
    private void findEncodes(String srcNumber, List<String> dstEncodingResultList) {
        String numberClean = Util.cleanNumber(srcNumber);
        this.globalEncodedDigits = new boolean[numberClean.length()];
        findEncodes(numberClean, dstEncodingResultList, new ArrayList<List<String>>(), 0, new boolean[numberClean.length()], false);
    }

    /**
     * The recursive method for findEncodes. The algorithm is like a
     * backtracking search, but the "globalEncodedDigits" flags are used to
     * populate encoded digits globally for each valid encoding. "number" will
     * be split into lookup entries to match with the dictionary hash table;
     * "dstEncodingResultList" is the final result list storing all the valid
     * encodes for the number; "currentEncodesList" stores matched encode lists
     * for the entries; "currentDigit" indicates which digit from the original
     * number we are searching for its encode; "encodedDigits" stores the flags
     * for found encoded digit, it will be populated to the globalEncodedDigits
     * if encoding process reaches the end; "previousDigitReplaced" indicates if
     * last recursive method has replaced an encode by the single digit itself
     */
    private void findEncodes(String number, List<String> dstEncodingResultList, List<List<String>> currentEncodesList, int currentDigit, boolean[] encodedDigits, boolean previousDigitReplaced) {
        for (String entry : Util.numberLookupEntries(number)) {
            if (dictionaryMap.containsKey(entry)) {
                List<List<String>> nextEncodesList = Util.deepCopyListOfStringList(currentEncodesList);
                nextEncodesList.add(dictionaryMap.get(entry));
                encodedDigits[currentDigit] = true; // mark the current digit to be true -> encoded
                int nextDigit = currentDigit + entry.length();
                String nextNumber = number.substring(entry.length());

                if (nextNumber.length() == 0) {
                    Util.combineListOfStringList(nextEncodesList, dstEncodingResultList);
                    this.globalEncodedDigits = Util.boolArraysBitwiseOR(encodedDigits, this.globalEncodedDigits); //populate encodedDigits into global one for a successful encoding
                } else {
                    findEncodes(nextNumber, dstEncodingResultList, nextEncodesList, nextDigit, encodedDigits, false);
                }
            } else if (entry.length() == 1 && previousDigitReplaced == false && this.globalEncodedDigits[currentDigit] == false) { // If no encoding found, handle the single digit case
                List<List<String>> nextEncodesList = Util.deepCopyListOfStringList(currentEncodesList);
                List<String> singleDigit = new ArrayList<>();
                singleDigit.add(entry);
                nextEncodesList.add(singleDigit);
                int nextDigit = currentDigit + entry.length();
                String nextNumber = number.substring(entry.length());

                if (nextNumber.length() == 0) {
                    Util.combineListOfStringList(nextEncodesList, dstEncodingResultList);
                    this.globalEncodedDigits = Util.boolArraysBitwiseOR(encodedDigits, this.globalEncodedDigits); //populate global flags for each successful encoding
                } else {
                    findEncodes(nextNumber, dstEncodingResultList, nextEncodesList, nextDigit, encodedDigits, true); // "true" means this time we put a single digit, so next single digit can't be used for replacing the encoding place
                }
            }
        }
    }
}
