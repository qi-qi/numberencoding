/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.numberencoding;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * This class contains utility methods.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Util {

    /**
     * Predefined Initial Mapping Map
     */
    private static final Map<Character, Character> mappingMap = Mapping.getMappingMap();

    /**
     * Translate a word into its numerical representation (String format)
     * according to the predefined letters to digits mapping. e.g. Torf -> 4824;
     * bo"s -> 783
     *
     * @param word An input string to be translated
     * @return The numerical representation (String format) of the word will be
     * returned
     */
    public static String wordToNumber(String word) {
        StringBuilder numberSB = new StringBuilder();
        for (char c : word.toCharArray()) {
            if (mappingMap.containsKey(c)) {
                numberSB.append(mappingMap.get(c));  //Transforms only valid characters
            }
        }
        return numberSB.toString();

    }

    /**
     * Read and transform the dictionary into a hash table in memory. The keys
     * are the numerical representation (String format) of words, and the values
     * are lists of words associated to the same key. e.g. {"51": ["an"],
     * "7857": ["blau"], "4824": ["Torf", "fort"]}
     *
     * @param dictionaryPath Dictionary words string array
     * @return the map transformed from the dictionary with numerical
     * representation (String format) of words for keys, and lists of words
     * associated to the same key for values.
     * @exception FileNotFoundException if the path of source is not found
     */
    public static Map<String, List<String>> loadDictionary(String dictionaryPath) throws FileNotFoundException {
        Map<String, List<String>> result = new HashMap<>();
        try (Scanner fileScanner = new Scanner(new File(dictionaryPath))) {
            while (fileScanner.hasNext()) {
                String word = fileScanner.next();
                String key = wordToNumber(word);
                if (result.containsKey(key)) {
                    result.get(key).add(word);
                } else {
                    List<String> wordList = new ArrayList<>();
                    wordList.add(word);
                    result.put(key, wordList);
                }
            }
            fileScanner.close();
        }
        return result;
    }

    /**
     * Clean the input number and extract the the pure numeric format. e.g.
     * "5624"-> "5624"; "10/783--5"-> "107835".
     *
     * @param number The input phone number needed to be cleaned
     * @return the pure numeric format of the number
     */
    public static String cleanNumber(String number) {
        StringBuilder numSB = new StringBuilder();
        for (char c : number.trim().toCharArray()) {
            if (Character.isDigit(c)) {
                numSB.append(c);
            }
        }
        return numSB.toString();
    }

    /**
     * Generate the look up entries for each input phone number. e.g. For number
     * "146288", its look up entries will be {146288, 14628, 1462, 146, 14, 1}
     *
     * @param number The input phone number needed to be encoded
     * @return The dictionary look up entries integer array related to the input
     * phone number
     */
    public static String[] numberLookupEntries(String number) {
        int numLength = number.length();
        String[] result = new String[numLength];
        for (int i = 0; i < result.length; i++) {
            result[i] = number.substring(0, numLength);
            numLength--;
        }
        return result;
    }

    /**
     * Combine (with space between Strings) a list of string lists. e.g. Source:
     * {{"aa","bb"},{"#"},{"1","2"}} -> Result: {"aa # 1", "aa # 2", "bb # 1",
     * "bb # 2"}
     *
     * @param srcListOfStringList Source list of String Lists to be permuted.
     * e.g. Source: {{"aa","bb"},{"#"},{"1","2"}}
     * @param dstCombinedResultList Result list of permuted (with space between
     * Strings) strings. e.g. Result: {"aa # 1", "aa # 2", "bb # 1", "bb # 2"}
     */
    public static void combineListOfStringList(List<List<String>> srcListOfStringList, List<String> dstCombinedResultList) {
        if (srcListOfStringList.size() == 1) {
            dstCombinedResultList.addAll(srcListOfStringList.get(0));
        } else {
            combineListOfStringList(srcListOfStringList, 1, srcListOfStringList.get(0), dstCombinedResultList);
        }
    }

    /**
     * Recursive method call for permuteListOfStringList. "currentIndex"
     * indicates the current permuting list index; "previousPermute" stores the
     * last permuted result. When "currentIndex" reaches the last permuting
     * list, final permuting result will be stored in "dstPermutedResultList"
     */
    private static void combineListOfStringList(List<List<String>> srcListOfStringList, int currentIndex, List<String> previousCombined, List<String> dstCombinedResultList) {

        List<String> currentStringList = new ArrayList<>();
        for (String stringPrevious : previousCombined) {
            for (String encodeCurrent : srcListOfStringList.get(currentIndex)) {
                currentStringList.add(stringPrevious + " " + encodeCurrent);
            }
        }
        if (srcListOfStringList.size() == currentIndex + 1) {
            dstCombinedResultList.addAll(currentStringList);
        } else {
            combineListOfStringList(srcListOfStringList, currentIndex + 1, currentStringList, dstCombinedResultList);
        }
    }

    /**
     * Deep copy a list of lists.
     *
     * @param srcListOfStringList The source of a list of lists.
     * @return The deep copy of a list of lists.
     */
    public static List<List<String>> deepCopyListOfStringList(List<List<String>> srcListOfStringList) {
        List<List<String>> result = new ArrayList<>();

        for (List<String> stringList : srcListOfStringList) {
            List<String> newStringList = new ArrayList<>();
            for (String stringEntity : stringList) {
                newStringList.add(stringEntity);
            }
            result.add(newStringList);
        }

        return result;
    }

    /**
     * Bitwise Inclusive OR operation on related position elements of boolean
     * array1 and array2. The length of array1 and array2 should be the same.
     * e.g. array1: {true, false, false}, array2: {false, false, true} ->
     * result: {true, false, true}
     *
     * @param array1 Boolean array1 to be bitwise inclusive OR with array2. The
     * length of array1 and array2 should be the same.
     * @param array2 Boolean array2 to be bitwise inclusive OR with array1. The
     * length of array1 and array2 should be the same.
     * @return The result boolean array.
     */
    public static boolean[] boolArraysBitwiseOR(boolean[] array1, boolean[] array2) {
        boolean[] result = new boolean[array1.length];
        for (int i = 0; i < array1.length; i++) {
            result[i] = array1[i] | array2[i];
        }
        return result;
    }
}
