/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.numberencoding;

import java.util.HashMap;
import java.util.Map;

/**
 * This class predefines the Initial Mapping from Letters to Digits.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Mapping {

    /**
     * Predefined Initial Mapping from Letters to Digits: 
     * E | J N Q | R W X | D S Y | F T | A M | C I V | B K U | L O P | G H Z
     * e | j n q | r w x | d s y | f t | a m | c i v | b k u | l o p | g h z
     * 0 |   1   |   2   |   3   |  4  |  5  |   6   |   7   |   8   |   9
     */
    private static final Map<Character, Character> mappingMap = new HashMap<Character, Character>(){{
        put('E', '0'); put('e', '0'); 
        put('J', '1'); put('N', '1'); put('Q', '1'); put('j', '1'); put('n', '1'); put('q', '1');
        put('R', '2'); put('W', '2'); put('X', '2'); put('r', '2'); put('w', '2'); put('x', '2');
        put('D', '3'); put('S', '3'); put('Y', '3'); put('d', '3'); put('s', '3'); put('y', '3');
        put('F', '4'); put('T', '4'); put('f', '4'); put('t', '4'); 
        put('A', '5'); put('M', '5'); put('a', '5'); put('m', '5'); 
        put('C', '6'); put('I', '6'); put('V', '6'); put('c', '6'); put('i', '6'); put('v', '6');
        put('B', '7'); put('K', '7'); put('U', '7'); put('b', '7'); put('k', '7'); put('u', '7');
        put('L', '8'); put('O', '8'); put('P', '8'); put('l', '8'); put('o', '8'); put('p', '8');
        put('G', '9'); put('H', '9'); put('Z', '9'); put('g', '9'); put('h', '9'); put('z', '9');
    }};

    /**
     * Get the the predefined initial mapping map.
     * Predefined Initial Mapping from Letters to Digits: 
     * E | J N Q | R W X | D S Y | F T | A M | C I V | B K U | L O P | G H Z
     * e | j n q | r w x | d s y | f t | a m | c i v | b k u | l o p | g h z
     * 0 |   1   |   2   |   3   |  4  |  5  |   6   |   7   |   8   |   9
     *
     * @return the predefined initial mapping map
     */
    public static Map<Character, Character> getMappingMap() {
        return mappingMap;
    }
}
