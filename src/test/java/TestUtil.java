/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.Test;
import static org.junit.Assert.*;
import com.numberencoding.Util;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class tests methods in com.numberencoding.Util.java class
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class TestUtil {

    /**
     * Predefined Initial Mapping from Letters to Digits: 
     * E | J N Q | R W X | D S Y | F T | A M | C I V | B K U | L O P | G H Z
     * e | j n q | r w x | d s y | f t | a m | c i v | b k u | l o p | g h z
     * 0 |   1   |   2   |   3   |  4  |  5  |   6   |   7   |   8   |   9
     */
    private static final String[] dictionary = {
        "an",
        "blau",
        "Bo\"",
        "Boot",
        "bo\"s",
        "da",
        "Fee",
        "fern",
        "Fest",
        "fort",
        "je",
        "jemand",
        "mir",
        "Mix",
        "Mixer",
        "Name",
        "neu",
        "o\"d",
        "Ort",
        "so",
        "Tor",
        "Torf",
        "Wasser"
    };

    private static final String[] dictionaryNumeric = {
        "51",
        "7857",
        "78",
        "7884",
        "783",
        "35",
        "400",
        "4021",
        "4034",
        "4824",
        "10",
        "105513",
        "562",
        "562",
        "56202",
        "1550",
        "107",
        "83",
        "824",
        "38",
        "482",
        "4824",
        "253302"
    };

    /**
     * Test wordToNumber with the dictionary string array and its related
     * numeric representation.
     */
    @Test
    public void wordToNumberTest() {
        for (int i = 0; i < dictionary.length; i++) {
            assertEquals(Util.wordToNumber(dictionary[i]), dictionaryNumeric[i]);
        }
    }

    /**
     * Test loadDictionary with predefined map. Map: {"51": ["an"], "7857":
     * ["blau"], "4824": ["Torf", "fort"]}
     *
     * @throws IOException If file write errors.
     */
    @Test
    public void loadDictionaryTest() throws IOException {
        final String[] words = {"an", "blau", "Torf", "fort"};
        final List<String> list1 = new ArrayList<String>() {
            {
                add("an");
            }
        };
        final List<String> list2 = new ArrayList<String>() {
            {
                add("blau");
            }
        };
        final List<String> list3 = new ArrayList<String>() {
            {
                add("Torf");
                add("fort");
            }
        };
        Map<String, List<String>> testMap = new HashMap<String, List<String>>() {
            {
                put("51", list1);
                put("7857", list2);
                put("4824", list3);
            }
        };

        try (FileWriter fw = new FileWriter("TestSmallDictionary.temp")) {
            for (String word : words) {
                fw.write(word + "\n");
            }
            fw.close();
        }

        Map<String, List<String>> resultMap = Util.loadDictionary("TestSmallDictionary.temp");
        assertEquals(testMap, resultMap);
        
        /**
         * Uncomment the following line if we want to delete the temp file
         * after testing
         */
        //Files.deleteIfExists(Paths.get("TestDictionary.temp")); // delete temporary dictionary file
    }

    /**
     * Test cleanNumber. The input is dirty number format "
     * 1--==4\\6!@#2--88??", " 146 28 8 " and also clean format "146288", the
     * result should be "146288"
     */
    @Test
    public void cleanNumberTest() {
        final String testNumber = "146288";
        final String resultNumber1 = Util.cleanNumber("  1--==4\\6!@#2--88??");
        final String resultNumber2 = Util.cleanNumber(" 146 28 8 ");
        final String resultNumber3 = Util.cleanNumber("146288");

        assertEquals(testNumber, resultNumber1);
        assertEquals(testNumber, resultNumber2);
        assertEquals(testNumber, resultNumber3);
    }

    /**
     * Test numberLookupEntries. The input is "146288", and the correct lookup
     * entries is {"146288", "14628", "1462", "146", "14", "1"}
     */
    @Test
    public void numberLookupEntriesTest() {
        final String[] testEntries = {"146288", "14628", "1462", "146", "14", "1"};
        final String[] resultEntries = Util.numberLookupEntries("146288");
        assertArrayEquals(testEntries, resultEntries);
    }

    /**
     * Test combineListOfStringList. The input is {{"aa","bb"},{"#"},{"1","2"}},
     * and the result should be {"aa # 1", "aa # 2", "bb # 1", "bb # 2"}
     */
    @Test
    public void combineListOfStringListTest() {
        final List<String> list1 = new ArrayList<String>() {
            {
                add("aa");
                add("bb");
            }
        };
        final List<String> list2 = new ArrayList<String>() {
            {
                add("#");
            }
        };
        final List<String> list3 = new ArrayList<String>() {
            {
                add("1");
                add("2");
            }
        };
        List<List<String>> srcList = new ArrayList<List<String>>() {
            {
                add(list1);
                add(list2);
                add(list3);
            }
        };

        List<String> testList = new ArrayList<String>() {
            {
                add("aa # 1");
                add("aa # 2");
                add("bb # 1");
                add("bb # 2");
            }
        };
        List<String> resultList = new ArrayList<>();
        Util.combineListOfStringList(srcList, resultList);

        assertEquals(testList, resultList);
    }

    /**
     * Test deepCopyListOfStringList. The values of the input and the result should be
     * the same, but point to different reference
     */
    @Test
    public void deepCopyListOfStringList() {
        final List<String> list1 = new ArrayList<String>() {
            {
                add("aa");
                add("bb");
            }
        };
        final List<String> list2 = new ArrayList<String>() {
            {
                add("#");
            }
        };
        final List<String> list3 = new ArrayList<String>() {
            {
                add("1");
                add("2");
            }
        };
        List<List<String>> testList = new ArrayList<List<String>>() {
            {
                add(list1);
                add(list2);
                add(list3);
            }
        };

        List<List<String>> resultList = Util.deepCopyListOfStringList(testList);
        // Values now should be equal
        assertEquals(testList, resultList);

        // Change the resultList
        resultList.get(0).add("ccc");
        // Values now should be different
        assertNotEquals(testList, resultList);
    }

    /**
     * Test boolArraysBitwiseOR. The length of array1 and array2 should be the
     * same. e.g. array1: {true, false, false}, array2: {false, false, true} ->
     * result: {true, false, true}
     */
    @Test
    public void boolArraysBitwiseORTest() {
        boolean[] array1 = {true, false, false};
        boolean[] array2 = {false, false, true};
        boolean[] testArray = {true, false, true};
        boolean[] resultArray = Util.boolArraysBitwiseOR(array1, array2);
        assertEquals(testArray.length, resultArray.length);
        for (int i = 0; i < testArray.length; i++) {
            assertEquals(testArray[i], resultArray[i]);
        }
    }
}
