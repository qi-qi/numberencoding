/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.numberencoding.Encoder;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class tests methods in com.numberencoding.Encoder.java class
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class TestEncoder {

    private static final String[] dictionary = {
        "an",
        "blau",
        "Bo\"",
        "Boot",
        "bo\"s",
        "da",
        "Fee",
        "fern",
        "Fest",
        "fort",
        "je",
        "jemand",
        "mir",
        "Mix",
        "Mixer",
        "Name",
        "neu",
        "o\"d",
        "Ort",
        "so",
        "Tor",
        "Torf",
        "Wasser"
    };

    private static final String[] input = {
        "112",
        "5624-82",
        "4824",
        "0721/608-4067",
        "10/783--5",
        "1078-913-5",
        "381482",
        "04824"
    };

    private static final Set<String> outputSet = new TreeSet<String>() {
        {
            add("5624-82: mir Tor");
            add("5624-82: Mix Tor");
            add("4824: Torf");
            add("4824: fort");
            add("4824: Tor 4");
            add("10/783--5: neu o\"d 5");
            add("10/783--5: je bo\"s 5");
            add("10/783--5: je Bo\" da");
            add("381482: so 1 Tor");
            add("04824: 0 Torf");
            add("04824: 0 fort");
            add("04824: 0 Tor 4");
        }
    };

    @BeforeClass
    public static void setUpClass() throws IOException {
        try (FileWriter fw1 = new FileWriter("TestInput.temp")) {
            for (String word : input) {
                fw1.write(word + "\n");
            }
            fw1.close();
        }

        try (FileWriter fw2 = new FileWriter("TestDictionary.temp")) {
            for (String word : dictionary) {
                fw2.write(word + "\n");
            }
            fw2.close();
        }
    }

    @AfterClass
    public static void tearDownClass() throws IOException {
        /**
         * Uncomment the following lines if we want to delete the temp files after testing
         */
        //Files.deleteIfExists(Paths.get("TestInput.temp")); // delete temporary input file
        //Files.deleteIfExists(Paths.get("TestDictionary.temp")); // delete temporary dictionary file 
        //Files.deleteIfExists(Paths.get("TestOutput.temp")); // delete temporary output file 
    }

    /**
     * Test the run method in Encoder. The result should be the same as the
     * predefined outputSet.
     * @throws IOException File Read / Wirte errors
     */
    @Test
    public void runTest() throws IOException {
        Encoder encoder = new Encoder("TestInput.temp", "TestDictionary.temp", "TestOutput.temp");
        encoder.run();

        List<String> resultList = Files.readAllLines(Paths.get("TestOutput.temp"), Charset.defaultCharset());

        assertEquals(outputSet.size(), resultList.size());

        Set<String> resultSet = new TreeSet<>(resultList);
        assertEquals(outputSet, resultSet);
    }
}
